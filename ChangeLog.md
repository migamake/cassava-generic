# Changelog for cassava-generics

# Version 0.1.0.2 July 30th 2020

* Updated metadata

# Version 0.1.0, July 30th, 2020

* First public release to Hackage.

# Version 0.0.2, June 2020

* Added generic instances for Identity functor.

# Version 0.0.1, April 2020

* Generic instances for Either and Maybe

## Unreleased changes


