# cassava-generics

Cassava `ToNamedField` instances for standard functors and datatypes.
They depend on both `ToNamedField` and `DefaultHeaderOrder` instances for `a`:

* `Either String a`
* `Maybe a`

Also for convenience instances for common types:
* [Text] -- tag list
* [(Text,Text)] -- assoc list

