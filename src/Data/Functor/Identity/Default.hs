module Data.Functor.Identity.Default(
  -- instances only
  ) where

import Data.Default
import Data.Functor.Identity

-- * Missing instances of `Data.Default.Default class`
instance Default a => Default (Identity a) where
  def = Identity def

